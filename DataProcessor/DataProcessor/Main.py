
from Data_processor import Data_processor
from Data_collection_engine import Data_collection_engine
import sys
import re
import multiprocessing
import threading

def main():
    
    if (len(sys.argv) != 2):
        print("Command line arguement missing. The path to the config file is required.")
        return

    with open(sys.argv[1]) as f:
        config_string = f.readlines()
        config_string = [x for x in config_string if not x.startswith("//")]
        config_string = "".join(config_string)

    procesing_retries = read_config_option("procesingRetries", config_string)
    num_data_processors = read_config_option("numDataProcessors", config_string)
    num_retry_processors = read_config_option("numRetryProcessors", config_string)
    processing_timeout = read_config_option("processingTimeout", config_string)
    start_timeout = read_config_option("startTimeout", config_string)
    chance_of_failure = read_config_option("chanceOfFailure", config_string)
    max_number_concurrent_processors = read_config_option("maxNumberConcurrentProcessors", config_string)
    data_url = read_config_option("dataUrl", config_string)
    output_file = read_config_option("outputFile", config_string)

    try:
        # clear file so multiple runs are easy to read
        open(output_file, 'w').close()
    except BaseException as e:
        print(f'Failed to clear output_file: {output_file}. Exception is: {e}')

    m = multiprocessing.Manager()
    lock = m.RLock()

    data_processors = create_processors(int(num_data_processors),
                                        int(processing_timeout),
                                        int(start_timeout),
                                        int(chance_of_failure),
                                        int(max_number_concurrent_processors),
                                        lock,
                                        output_file)

    retry_processors = create_processors(int(num_retry_processors),
                                        int(processing_timeout),
                                        int(start_timeout),
                                        int(chance_of_failure),
                                        int(max_number_concurrent_processors),
                                        lock,
                                        output_file)

    engine = Data_collection_engine(procesing_retries, data_processors, retry_processors, data_url, lock, output_file)
    engine.start_processing()

    
def read_config_option(option: str, config_string: str, pattern:str = None) -> str:

    procesingRetries = re.search(f'{option}=([0-9a-zA-Z-_./:]+)', config_string)
   
    if procesingRetries:
        return procesingRetries.group(1)

    raise Exception(f'config option {option} not found in config file. Program terminating')

def create_processors(num_processors: int,
                      processing_timeout: int,
                      start_timeout: int,
                      chance_of_failure: int,
                      max_number_of_concurrent_processors: int,
                      lock: threading.RLock,
                      output_file) -> Data_processor:

    processors = []
    for index in range(num_processors):
        processors.append(Data_processor(processing_timeout, 
                                         start_timeout, 
                                         chance_of_failure, 
                                         max_number_of_concurrent_processors,
                                         lock,
                                         output_file))

    return processors

if __name__ == "__main__":
    main()


