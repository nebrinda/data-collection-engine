from concurrent.futures import ThreadPoolExecutor
import threading
import random
import datetime
import time
import json
import Record
import typing

class Data_processor(object):

    def __init__(self, 
                 processing_timeout: int, 
                 start_timeout: int, 
                 chance_of_failure: int, 
                 max_number_concurrent_processors: int, 
                 lock: threading.RLock,
                 output_file: str):

        self._processing_timeout: int = processing_timeout
        self._start_timeout: int = start_timeout
        self._chance_of_failure: int = chance_of_failure
        self._max_number_concurrent_processors: int = max_number_concurrent_processors
        self._number_of_processing_threads: int = 0
        self._output_file: str = output_file
        self._lock: threading.RLock = lock

        #double the threads specified, as each thread spawns one of it's own to handle timing out the processing piece
        self._thread_pool = ThreadPoolExecutor(max_number_concurrent_processors * 2)
        
    def add(self, record: Record, retry_record: callable, record_processed: callable) -> None:

        try:
            with self._lock:
                if (self._number_of_processing_threads < self._max_number_concurrent_processors):
                    self._thread_pool.submit(self._process_record, record, retry_record, record_processed)
                else:
                    self._thread_pool.submit(self._handle_queued_record, record, retry_record, record_processed, datetime.datetime.now())
        except error:
            print(f'Failed to handle record with id: {record.json_id}')

    def _process_record(self, record : Record, retry_record: callable, record_processed: callable) -> None:

        with self._lock: self._number_of_processing_threads += 1 
        my_list = [False] * (100 - self._chance_of_failure) + [True] * self._chance_of_failure

        if (random.choice(my_list)):
            with self._lock: self._number_of_processing_threads -= 1
            retry_record(record, self._lock)
            return
        else:
            processing_thread = self._thread_pool.submit(Data_processor.write_to_file, record, "true", record_processed, self._is_first_record, self._is_last_record, self._lock, self._output_file)

        finished_processing = processing_thread.result(self._processing_timeout)
        with self._lock: self._number_of_processing_threads -= 1

        if not finished_processing:
            print(f'Record with id: {record.json_id} timed out. Retrying.')
            retry_record(record, self._lock)
                
    def _handle_queued_record(self, record: Record, retry_record: callable, record_processed: callable, queued_time: datetime.datetime) -> None:

        print(f'record with id: {record.json_id} being queued for processing')
        
        is_queued = False
        test1 = datetime.datetime.now()
        test2 = queued_time + datetime.timedelta(seconds=self._start_timeout)
        while (datetime.datetime.now() < (queued_time + datetime.timedelta(seconds=self._start_timeout))):
            with self._lock: processing_thread_count = self._number_of_processing_threads
            if (processing_thread_count <  self._max_number_concurrent_processors):
                print(f'Processor available, subimiting record with id: {record.json_id} for processing')
                self._thread_pool.submit(self._process_record, record, retry_record, record_processed)
                is_queued = True
                break;
            time.sleep(1)
       
        if not is_queued:
            retry_record(record, self._lock)

    @staticmethod
    def write_to_file(record: Record, processed: str, record_processed: callable, is_first: bool, is_last: bool, lock: threading.RLock, output_file) -> bool:
        
        with lock:
            try:
                json_data = record.data
                today = datetime.datetime.now()
                json_data.update({'processing_date':f'{today.strftime("%m-%d-%YT%H:%M:%S.%f")}'})
                json_data.update({'processed':f'{processed}'})
                string_record = json.dumps(json_data)

                if is_first():
                    string_record = '[' + string_record
                if not is_last():
                    string_record = string_record + ','
                if is_last():
                    string_record = string_record + ']'
                with open(output_file, 'a') as outfile:
                    outfile.write(string_record)
                    record_processed(record, lock)
                return True
            except BaseException as e:
                print(f'failed to write json update to file. Exception is {e}')
                return False

    def can_process(self) -> bool:
        with self._lock: 
            return self._number_of_processing_threads < self._max_number_concurrent_processors

    @property
    def get_thread_pool(self) -> int:
        return self._thread_pool

    @property
    def get_num_processing_threads(self) -> int:
        return self._number_of_processing_threads

    def set_is_first_record(self, is_first_record: callable):
        self._is_first_record = is_first_record

    def set_is_last_record(self, is_last_record: callable):
        self._is_last_record = is_last_record
