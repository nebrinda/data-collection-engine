
import unittest
from Main import read_config_option
from Main import create_processors
import multiprocessing

class Test_main(unittest.TestCase):
    
    def test_read_config_option(self):
        
        num_retries = read_config_option("procesingRetries", "procesingRetries=3")
        self.assertEqual(int(num_retries), 3)
    
        try:
            read_config_option("123abc", "procesingRetries=3")
            exception_thrown = False
        except:
            exception_thrown = True

        self.assertTrue(exception_thrown)

    def test_create_processors(self):
        
        data_processors = create_processors(10, 10, 5, 25, 5, multiprocessing.Manager().RLock(), 'unused')
        self.assertEqual(len(data_processors), 10)

        for processor in data_processors:
            self.assertEqual(processor._processing_timeout, 10)
            self.assertEqual(processor._start_timeout, 5)
            self.assertEqual(processor._chance_of_failure, 25)
            self.assertEqual(processor._max_number_concurrent_processors, 5)

        no_processors = create_processors(0, 0, 0, 0, 0, multiprocessing.Manager().RLock(), 'unused')
        self.assertEqual(len(no_processors), 0)
 

if __name__ == '__main__':
    unittest.main()
