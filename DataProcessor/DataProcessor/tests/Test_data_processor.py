import unittest
from unittest.mock import patch
from unittest.mock import ANY
from Data_processor import Data_processor
from Record import Record
import time
import multiprocessing

class Test_data_processor(unittest.TestCase):

    _lambda1 = lambda a, b: (a, b)
    _lambda2 = lambda c, d: (c, d)
    _record = Record(1, { "1":"3" }, "test")
    _retry_was_called = False

    def test_process_record_success(self):

        # testing the case where there is space to immediately process a record without failure
        with patch.object(Data_processor, "_process_record") as mock_process_record:
            Data_processor(1, 2, 0, 5, multiprocessing.Manager().RLock(), 'unused').add(self._record, self._lambda1, self._lambda2)
            time.sleep(2)
            mock_process_record.assert_called_once_with(self._record, self._lambda1, self._lambda2)

    def test_record_fails_from_chance(self):
        
        #testing the case where there is space to process a record but it fails
        Data_processor(5, 5, 100, 5, multiprocessing.Manager().RLock(), 'unused').add(self._record, self._retry, self._lambda2)
        time.sleep(2)
        self.assertTrue(self._retry_was_called)
        _retry_was_called = False

    def test_record_fails_from_max_capacity(self):

        # testing the case where there is NOT space to process a record
        with patch.object(Data_processor, "_handle_queued_record") as mock_handle_queued_record:
            processor = Data_processor(1, 1, 0, 1, multiprocessing.Manager().RLock(), 'unused')
            processor._number_of_processing_threads = 1
            processor.add(self._record, self._retry, self._lambda2)
            time.sleep(2)
            mock_handle_queued_record.assert_called_once_with(self._record, self._retry, self._lambda2, ANY)

    def test_record_fails_from_timeout(self):

        # testing the case where there is NOT space to process a record and a timeout occures
        processor = Data_processor(1, 1, 0, 1, multiprocessing.Manager().RLock(), 'unused')
        processor._number_of_processing_threads = 1
        processor.add(self._record, self._retry, self._lambda2)
        time.sleep(2)
        self.assertTrue(self._retry_was_called)
        self._retry_was_called = False

    def test_queued_record(self):

        # testing the case where there is NOT space to process a record but space is recovered after first failure
        with patch.object(Data_processor, "_process_record") as mock_process_record:
            processor = Data_processor(1, 5, 0, 1, multiprocessing.Manager().RLock(), 'unused')
            processor._number_of_processing_threads = 1
            processor.add(self._record, self._retry, self._lambda2)
            time.sleep(1)
            processor._number_of_processing_threads = 0
            
            time.sleep(2)
            mock_process_record.assert_called_once_with(self._record, self._retry, self._lambda2)
            self.assertFalse(self._retry_was_called)
            self._retry_was_called = False

    def _retry(self, _, _1) -> None:
        self._retry_was_called = True

if __name__ == '__main__':
    unittest.main()

