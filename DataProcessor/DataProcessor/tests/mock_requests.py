import json
import unittest

class Mock_requests(unittest.TestCase):

    def mocked_requests_get_all(self, *args, **kwargs):
        class MockResponse:
            def __init__(self, content, status_code):
                self.content = content
                self.status_code = status_code

        with open('test_json', 'r') as f:
            test_json = json.loads(f.read())
        return MockResponse(json.dumps(test_json), 200)

    def mocked_requests_get_one(self, *args, **kwargs):
        class MockResponse:
            def __init__(self, content, status_code):
                self.content = content
                self.status_code = status_code

        test_json = json.loads(r"""
                [
	                   {
		                  "id": "924c8cfbd9f94155985bf262cf2c3c67",
		                  "source": "MessagingSystem",
		                  "title": "Where are my pants?",
		                  "creation_date": "2030-08-24T17:16:52.228009",
		                  "message": "Erlang is known for its designs that are well suited for systems. Haskell features a type system with type inference and lazy evaluation. Messages can be sent to and received from ports, but these messages must obey the so-called \"port protocol.\" Atoms can contain any character if they are enclosed within single quotes and an escape convention exists which allows any character to be used within an atom. It is also a garbage-collected runtime system.",
		                  "tags": [
			                 "no",
			                 "collection",
			                 "building",
			                 "building",
			                 "seeing"
		                  ],
		                  "author": "Dominic Mccormick"
	                   }
                ]
                """)

        return MockResponse(json.dumps(test_json), 200)
