
import unittest
from unittest.mock import patch
from unittest.mock import ANY
from Data_processor import Data_processor
import json
from tests.mock_requests import Mock_requests
import multiprocessing
from Data_collection_engine import Data_collection_engine
from Record import Record
from typing import List, Any

class Test_data_collection_engine(unittest.TestCase):
        
    @patch('Data_collection_engine.Data_collection_engine._update_processor_index')
    @patch('Data_collection_engine.Data_collection_engine._get_next_processor')
    @patch('requests.get')
    @patch('Data_processor.Data_processor')
    def test_start_processing_success(self, mock_data_processor, mock_requests_get, mock_get_next_processor, mock_update_processor_index):

        # tests the case where starting procesing succeeds
        mock_requests_get.side_effect = Mock_requests.mocked_requests_get_one
        mock_get_next_processor.return_value = (0, mock_data_processor)
        mock_update_processor_index.return_value = 1
        
        engine = Data_collection_engine(3, [ mock_data_processor ], [ mock_data_processor ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')

        mock_data_processor.add.side_effect = engine._handle_record_processed(Record(0, "", ""), multiprocessing.Manager().RLock())
        
        engine.start_processing()

        mock_data_processor.add.assert_called_once()
        mock_get_next_processor.assert_called_once()
        mock_update_processor_index.assert_called_once()
    
    @patch('Data_collection_engine.Data_collection_engine._handle_failed_record')
    @patch('Data_collection_engine.Data_collection_engine._update_processor_index')
    @patch('Data_collection_engine.Data_collection_engine._get_next_processor')
    @patch('requests.get')
    @patch('Data_processor.Data_processor')
    def test_start_processing_without_processor(self, mock_data_processor, mock_requests_get, mock_get_next_processor, mock_update_processor_index, mock_handle_failed_record):

        # tests the case where starting process fails to find an available processor
        mock_requests_get.side_effect = Mock_requests.mocked_requests_get_one
        mock_get_next_processor.return_value = None, None
        mock_update_processor_index.return_value = 1

        engine = Data_collection_engine(3, [ mock_data_processor ], [ mock_data_processor ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')
        mock_handle_failed_record.side_effect = engine._handle_record_processed(Record(0, "", ""), multiprocessing.Manager().RLock())
        engine.start_processing()
        
        mock_get_next_processor.assert_called_once()
        mock_update_processor_index.assert_called_once()
        mock_handle_failed_record.assert_called_once()

    @patch('Data_processor.Data_processor')
    @patch('Data_processor.Data_processor')
    def test_all_processors_busy(self, mock_data_processor1, mock_data_processor2):
        
        engine = Data_collection_engine(3, [ mock_data_processor1 ], [ mock_data_processor1 ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')
        
        # all processors occupied
        mock_data_processor1.can_process.return_value = False
        mock_data_processor2.can_process.return_value = False

        key, processor = engine._get_next_processor(0, { 0 : mock_data_processor1, 1 : mock_data_processor2 })

        self.assertTrue(processor is None)
        self.assertTrue(key is None)

    @patch('Data_processor.Data_processor')
    @patch('Data_processor.Data_processor')
    def test_next_processor_index_busy(self, mock_data_processor1, mock_data_processor2):
        
        engine = Data_collection_engine(3, [ mock_data_processor1 ], [ mock_data_processor1 ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')
        
        # index points at occupied processor, grab the next available one
        mock_data_processor1.can_process.return_value = False
        mock_data_processor2.can_process.return_value = True

        key, processor = engine._get_next_processor(0, { 0 : mock_data_processor1, 1 : mock_data_processor2 })

        self.assertEqual(processor, mock_data_processor2)
        self.assertEqual(key, 1)
        
    @patch('Data_processor.Data_processor')
    def test_next_processor_bad_index(self, mock_data_processor):
        
        engine = Data_collection_engine(3, [ mock_data_processor ], [ mock_data_processor ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')

        mock_data_processor.can_process.return_value = True
        key, processor = engine._get_next_processor(5, { 0 : mock_data_processor })

        self.assertTrue(processor is None)
        self.assertTrue(key is None)

    @patch('Data_collection_engine.Data_collection_engine._handle_failed_record')
    @patch('Data_processor.Data_processor')
    def test_retry_record_exceeded(self, mock_data_processor, mock_handle_failed_record):
        
        engine = Data_collection_engine(1, [ mock_data_processor ], [ mock_data_processor ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')
        engine._failed_records = {0 : 2}
        
        record = Record(0, "", "")
        engine._retry_record(record, multiprocessing.Manager().RLock())

        mock_handle_failed_record.assert_called_once_with(record)
    
    @patch('Data_collection_engine.Data_collection_engine._update_processor_index')
    @patch('Data_processor.Data_processor')
    def test_retry_record(self, mock_data_processor, mock_update_processor_index):
        
        engine = Data_collection_engine(1, [ mock_data_processor ], [ mock_data_processor ], 'http://badurl', multiprocessing.Manager().RLock(), 'unused')
        
        record = Record(0, "", "")
        engine._retry_record(record, multiprocessing.Manager().RLock())

        mock_data_processor.add.assert_called_once_with(record, ANY, ANY)
        mock_update_processor_index.assert_called_once_with(0, 1)
        
if __name__ == '__main__':
    unittest.main()
