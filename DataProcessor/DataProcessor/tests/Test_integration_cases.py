from typing import Any, List
import unittest
from unittest.mock import patch
import Main
from tests.mock_requests import Mock_requests
import json
import os

class Test_integration_cases(unittest.TestCase):
    
    @patch('requests.get')
    def test_all_fail(self, mock_requests_get: Any) -> None:
        
        mock_requests_get.side_effect = Mock_requests.mocked_requests_get_all
        test_output_file = 'test_output.json'

        self._create_test_config(['procesingRetries=3\n',
                             'numDataProcessors=4\n',
                             'numRetryProcessors=1\n',
                             'dataUrl=https://bad_url\n', #this url is never used, the get request is mocked out
                             'processingTimeout=7\n',
                             'startTimeout=2\n',
                             'chanceOfFailure=100\n',
                             'maxNumberConcurrentProcessors=5\n',
                             f'outputFile={test_output_file}'])
            
        with unittest.mock.patch('sys.argv', ['-a', 'test_config']):
            Main.main()

        with open(test_output_file, 'r') as f:
            results = json.loads(f.read())

        for result in results:
            was_processed = result.get('processed')
            self.assertEquals(was_processed, 'false')

        open(test_output_file, 'w').close()

    @patch('requests.get')
    def test_all_success(self, mock_requests_get: Any) -> None:
        
        mock_requests_get.side_effect = Mock_requests.mocked_requests_get_all
        test_output_file = 'test_output.json'
        
        self._create_test_config(['procesingRetries=3\n',
                             'numDataProcessors=4\n',
                             'numRetryProcessors=1\n',
                             'dataUrl=https://bad_url\n', #this url is never used, the get request is mocked out
                             'processingTimeout=1000\n',
                             'startTimeout=2\n',
                             'chanceOfFailure=0\n',
                             'maxNumberConcurrentProcessors=1\n',
                             f'outputFile={test_output_file}'])
            
        with unittest.mock.patch('sys.argv', ['-a', 'test_config']):
            Main.main()

        with open(test_output_file, 'r') as f:
            results = json.loads(f.read())

        for result in results:
            was_processed = result.get('processed')
            self.assertEquals(was_processed, 'true')

        open(test_output_file, 'w').close()
        
    def _create_test_config(self, args: List[str]) -> None:
        try:
            with open('test_config', 'w+') as f:
                f.writelines(args)
        except BaseException as e:
            print(f'Failed to create test config file. Exception is: {e}')
            
if __name__ == '__main__':
    unittest.main()
