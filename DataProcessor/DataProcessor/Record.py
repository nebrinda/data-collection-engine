class Record(object):
     
    def __init__(self, id: int, data: str, json_id: str) -> None:
        self._data = data
        self._id = id
        self._json_id = json_id

    @property
    def data(self) -> str:
        return self._data

    @data.setter
    def data(self, data: str) -> None:
        self._data = data

    @property
    def id(self) -> int:
        return self._id

    @id.setter
    def id(self, id: int) -> None:
        self._id = id

    @property
    def json_id(self) -> str:
        return self._json_id

    @json_id.setter
    def json_id(self, json_id: str) -> None:
        self._json_id = json_id

