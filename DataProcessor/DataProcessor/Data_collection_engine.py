
from Data_processor import Data_processor
from Record import Record
from typing import Dict, List, Tuple
import requests
import json
import threading
import time

class Data_collection_engine(object):
    
    def __init__(self, procesessing_retries: int, data_processors: List[Data_processor], retry_processors: List[Data_processor], data_url: str, lock: threading.RLock, output_file: str):

        self._next_data_processor_index: int = 0
        self._next_retry_processor_index: int = 0
        self._records_processed: int = 0
        self._procesessing_retries: int = int(procesessing_retries)
        self._data_url: str = data_url
        self._data_processors: Dict[int, Data_processor] = self._create_processor_dictionary(data_processors)
        self._retry_processors: Dict[int, Data_processor] = self._create_processor_dictionary(retry_processors)
        self._failed_records: Dict[int, int] = {}
        self._total_records: int = 0
        self._lock: threading.RLock = lock
        self._output_file: str = output_file

    def start_processing(self) -> None:

        try:
            resp = requests.get(self._data_url)
            data = json.loads(resp.content)
        except Exception as e:
            print(f'Failed to retrieve json from {self._data_url}. Exception is {e}. Response status code is: {resp.status_code} and reason is: {resp.reason}')
            raise SystemExit(e)

        self._total_records = len(data)

        for string_record in data:
            key, processor = self._get_next_processor(self._next_data_processor_index, self._data_processors)
            self._next_data_processor_index = self._update_processor_index(self._next_data_processor_index, len(self._data_processors))

            rec = Record(self._total_records, string_record, string_record.get('id'))
            if processor is None:
                self._handle_failed_record(rec)
            else:
                print(f'Adding record with id: {rec.json_id} to processor with id: {key}')
                self._write_processor_states()
                processor.add(rec, self._retry_record, self._handle_record_processed)

        # don't shut down the main threaed until we've processed all the records.
        while (self._records_processed < self._total_records):
            time.sleep(3)

        self._shutdown_processors(self._data_processors)
        self._shutdown_processors(self._retry_processors)

    def _handle_failed_record(self, rec: Record) -> None:
        Data_processor.write_to_file(rec, "false", self._handle_record_processed, self._writing_first_record, self._writing_last_record, self._lock, self._output_file)
    
    def _create_processor_dictionary(self, processors: List[Data_processor]) -> Dict[int, Data_processor]:
        
        processors_dict = {}
        counter = 0
        for processor in processors:
            processor.set_is_first_record(self._writing_first_record)
            processor.set_is_last_record(self._writing_last_record)
            processors_dict[counter] = processor
            counter += 1

        return processors_dict

    def _get_next_processor(self, _next_processor_index, processors) -> Tuple[int, Data_processor]:
        with self._lock:
            processor = processors.get(_next_processor_index)
            key = _next_processor_index

            # we found a data processor
            if processor is not None:
                # the processor we found is at max capacity. Try and find another one
                if not processor.can_process():
                    key = processor = None
                    for k, p in processors.items():
                        if p.can_process():
                            processor = p
                            key = k
            else:
                key = processor = None
                print(f'cannot find data processor with key {self._next_data_processor_index}')

            return key, processor

    def _update_processor_index(self, processor_index: int, processor_index_upper_bound: int) -> int:

        processor_index += 1
        if (processor_index > processor_index_upper_bound - 1):
            processor_index = 0
        return processor_index

    def _shutdown_processors(self, processors) -> None:
        for processor in processors.values():
            processor.get_thread_pool.shutdown(True)

    def _handle_record_processed(self, record: Record, lock) -> None:
        with lock:
            self._records_processed += 1
            self._failed_records.pop(record.id, None)
  
    def _retry_record(self, record: Record, lock) -> None:
        
        with lock:
            num_failed_retries = self._failed_records.get(record.id)
            if(num_failed_retries is None):
                num_failed_retries = 0
                self._failed_records[record.id] = 0

            key, processor = self._get_next_processor(self._next_retry_processor_index, self._retry_processors)
            
            if processor is None or num_failed_retries >= self._procesessing_retries:
                print(f'record with id {record.json_id} hit max retries. Failed to process')
                self._handle_failed_record(record)
            else:
                self._failed_records[record.id] += 1
                print(f'Retrying record with id: {record.json_id} on processor with id {key}')
                processor.add(record, self._retry_record, self._handle_record_processed)
                self._next_retry_processor_index = self._update_processor_index(self._next_retry_processor_index, len(self._retry_processors))

            self._write_processor_states()

    def _write_processor_states(self) -> None:
        pass
        with self._lock:

            for key, processor in self._data_processors.items():
                print(f'Data processor with id: {key} is currently processing: {processor.get_num_processing_threads} record(s)')

            for key, processor in self._retry_processors.items():
                print(f'Retry processor with id: {key} is currently processing: {processor.get_num_processing_threads} record(s)')    
                
    def _writing_first_record(self) -> bool:
        with self._lock: return self._records_processed == 0 
        
    def _writing_last_record(self) -> bool:
        with self._lock: return self._records_processed == (self._total_records - 1)